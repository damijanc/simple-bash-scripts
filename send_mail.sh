#!/bin/bash

#variables
PROGNAME=$(basename $0)
FROM=             #file to process
TO=  #output file
SUBJECT=         #echo to screen
BODY=
declare -a ATTACHMENTS
BOUNDARY=="ZZ_/afg6432dfgkl.94531q"
VERBOSE=0 #use to enable debug output


## Usage
function usage() {
cat << CIAO
NAME
    $PROGNAME - send email using sendmail

SYNOPSIS
   ./$PROGNAME -f [from] -t [to] -s [subject] -b [body] -a [attachment] [attachment] ...

DESCRIPTION
   Sends email using send mail

   Mandatory arguments to long options are mandatory for short options too.

   -f, --from sender email address
   -t, --to comma separated list of recepients emails
   -s, --subject mail subject
   -b, --body message body
   -a, --attachment whitespace list of file attachments
   -v, --verbose   put output also to console
   -h, --help   display this help and exit

EXAMPLES
   Send mail 
   /$PROGNAME -f me@example.com -t you@example.com -s 'This is a test' -b 'This is a test body' -a /tmp/file1.txt /tmp/file2.txt

AUTHOR
     Written by Damijan Ćavar

CIAO
}

## Read input char
function readchar() {
    local __result=$1
    echo -en $2
    read -n 1 input
    eval $__result="'$input'"
    return 0
}


##validate input 
function validate_input() {
    # Exit if there is some invalid arguments
    if [ "$1" = "-h" ]; then
        usage
		echo 'validation failed'
        exit 0
    fi

    if [ $# -lt 1 ]; then
        usage
		echo 'validation failed'
        exit 0
    fi
}


#check user input
validate_input $@

#get the options

while [ $# -ne 0 ]; do
    #echo 'testing variable ' "$1"
    case "$1" in
          -f | --from )
            FROM="$2"
            shift 2
            ;;
		  -t | --to )
            TO="$2"
            shift 2
            ;;
		  -s | --subject )
            SUBJECT="$2"
            shift 2
            ;;
		  -b | --body )
            BODY="$2"
            shift 2
            ;;
		  -a | --attachment)
		    shift 1
            TOTAL_PARAMETERS=$#
			COUNT=0
			while [ $COUNT -lt $TOTAL_PARAMETERS ]
			do
				if [ ! -n "$1" ] #if we have no more parameters
				then
                    #echo "no more options"
					break
				fi
				
				PAR=$1 #get the parameter
				FC="${PAR%?}" #get the first char of the parameter

				if [ $FC = '-' ]  #if another option
				then
                    #echo 'no more attachments'
					break
				fi
				
				ATTACHMENTS+=($1) #add attachment to array
				let COUNT=COUNT+1  #increase the counter
				shift 1 
                
			done
			;;
          -h | --help)
            usage
            exit 0
            ;;
          -v | --verbose)
            VERBOSE=1
            shift
            ;;
          -*)
            echo "Error: Unknown option: $1" >&2
            exit 1
            ;;
          *)  # Unrecognised options
            echo "Unrecognised option: $1"
            usage
            exit 1
            ;;
    esac
done

if [ "$VERBOSE" -eq 1 ]
then
    echo 
    echo 'Options collected'
    echo "FROM: $FROM "
    echo "TO: $TO "
    echo "SUBJECT: $SUBJECT "
    #debuging the options
    echo -n 'ATTACHMENTS '
	for attachment in "${ATTACHMENTS[@]}"
	do
		echo -n " $attachment "
	done
    echo
    echo
fi

# Build headers
{
printf '%s\n' "From: $FROM
To: $TO
Subject: $SUBJECT
Mime-Version: 1.0
Content-Type: multipart/mixed; boundary=\"$BOUNDARY\"

--${BOUNDARY}
Content-Type: text/plain; charset=\"US-ASCII\"
Content-Transfer-Encoding: 7bit
Content-Disposition: inline

$BODY
"


# now loop over the attachments, guess the type
# and produce the corresponding part, encoded base64
for file in "${ATTACHMENTS[@]}"; do

      [ ! -f "$file" ] && echo "Warning: attachment $file not found, skipping" >&2 && continue

mt=`file -i /tmp/lala.sql | awk '{print $2}'`
mimetype="${mt%?}"

base=`basename $file`

printf '%s\n' "--${BOUNDARY}
Content-Type: $mimetype
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename=\"$base\"
"

base64 "$file"
echo
done

# print last boundary with closing --
printf '%s\n' "--${BOUNDARY}--"

} | sendmail -t -oi  && echo 'Please check mail queue (mailq) if email is delivered.'
