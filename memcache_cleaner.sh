#!/bin/bash

_PROGNAME=$(basename $0)

#defaults
_TARGET=127.0.0.1
_PORT=11211

## Usage
function usage() {
#clear
cat << DELO
NAME
       $_PROGNAME - clears memcache based on prefix

SYNOPSIS
       ./$_PROGNAME PREFIX [-p|--port] PORT [-t|--target] TARGET

DESCRIPTION
       Clears memcache on host and port based on prefix .

       Mandatory arguments to long options are mandatory for short options too.

       -t, --target server host - defaults to 127.0.0.1
       -p, --port defaults to 11211
       -h, --help display this help and exit

EXAMPLES
       Deploy to production from tag on master branch and make db backup
           /$_PROGNAME dur -t 127.0.0.1 -p 11211

AUTHOR
       Written by Tim Rijavec, Damijan Ćavar

DELO
}

function validate_input() {
   # Exit if there is some invalid arguments
   if [ $# -lt 1 ]; then
      usage
      exit 0
   fi
}

## Read input char
function readchar() {
   local __result=$1
   echo -en $2
   read -n 1 input
   eval $__result="'$input'"
   return 0
}


validate_input $@

SEARCH_KEY_PREFIX=$1
shift

# Settings
while [ $# -ne 0 ]; do
   case "$1" in
      -t|--target)
         _TARGET="$1"
         shift
         ;;
      -p|--port)
         _PORT="$1"
         shift
         ;;
      -h|--help)
         usage;
         exit 1
         ;;
      (--) usage; exit 1;;
      (-*) usage; exit 1;;
      (*)
          usage;
          exit 1
        ;;
   esac
done

readchar input "You are about to delete memcache for prefix $SEARCH_KEY_PREFIX on host $_HOST and port $_PORT. Are you sure? [N/y]: "
  read -n 1
  if [ "$input" == "y" ] || [ "$input" == "Y" ];
  then

    for i in `echo 'stats items' | nc $_TARGET $_PORT | cut -d":" -f 2 | head -n -1 | uniq`; do
    for j in `echo "stats cachedump $i 100" | nc $_TARGET $_PORT`; do
      #if [[ "$j" == "$SEARCH_KEY_PREFIX"* ]]
      #then
        key=$j;
        echo "Deleting key: $key";
        echo "delete $key" | nc $_TARGET $_PORT;
     ## fi
    done
  done
fi
