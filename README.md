# README #

This is a repository of unsorted bash scripts

* ini_copy - copies and renames development ini files and replaces the production ones
* url_test - check a list of urls for 404 errors
* lock-execution-until-done - locks execution of a script until it is done. usefull for cron jobs
* memcache_cleaner - clears memcache based on prefix, NOT OFICALLY suported by Memcached
* send_mail - send email using sendmail with attachment support
* ...