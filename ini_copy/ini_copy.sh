#!/bin/bash

#variables
_PROGNAME=$(basename $0)
_INIT_FOLDER=.
_OUPUT_FOLDER=..
_RECURSIVE=0


#functions

## Usage
function usage() {
#clear
cat << CIAO
NAME
       $_PROGNAME - copies and renames development ini files and replaces the production

SYNOPSIS
       ./$_PROGNAME SUBSTRING [-f|--from] FROM [-t|--to] TO

DESCRIPTION
       copies and renames development ini files and replaces existing ones in the tagget folder

       Mandatory arguments to long options are mandatory for short options too.

       -f, --from folder to look for ini files - defaults to current folder 
       -t, --to folder to copy files to -  default to parent folder
       -r, --recursive, search for ini files recursively in from folder , NOT IMPLEMENTED YET
       -h, --help display this help and exit

EXAMPLES
       Copy file *.override.ini from folder /tmp/test to folder /tmp/test_output and remove .override from the filename
           /$_PROGNAME '.override' -f /tmp/test -t /tmp/test_output

AUTHOR
       Written by Damijan Ćavar

CIAO
}

##validate input 
function validate_input() {
   # Exit if there is some invalid arguments
   if [ $# -lt 1 ]; then
      usage
      exit 0
   fi
}

## Read input char
function readchar() {
   local __result=$1
   echo -en $2
   read -n 1 input
   eval $__result="'$input'"
   return 0
}


validate_input $@

REPLACEMENT_STRING=$1
shift

# Settings
while [ $# -ne 0 ]; do
   case "$1" in
      -t|--to)
         _OUPUT_FOLDER="$1"
         shift
         ;;
      -f|--from)
         _INIT_FOLDER="$1"
         shift
         ;;
       -r|--recursive)
         _RECURSIVE=1
         ;;   
      -h|--help)
         usage;
         exit 1	
         ;;
      (--) usage; exit 1;;
      (-*) usage; exit 1;;
      (*)
          usage;
          exit 1
        ;;
   esac
done

readchar input "You are copy and replace the ini configuration files from  $_INIT_FOLDER to $_OUPUT_FOLDER and delete $REPLACEMENT_STRING from the filename. Are you sure? [N/y]: "
  read -n 1
  if [ "$input" == "y" ] || [ "$input" == "Y" ];
  then
    for f in *.ini #loop trough the files
    do 
	#echo "Processing $f file..";
	#echo ${f/$SEARCH_KEY_PREFIX/}
	#check if it is a file
	if [ -f $f ]
	then
	  mv -i $f  $_OUPUT_FOLDER/${f/$SEARCH_KEY_PREFIX/}
	fi
    done
  fi

