#!/bin/bash

#cron:
#00-59/1 * * * *  cd /var/scripts/cron && ./Packager.sh


LOCK=/tmp/.lock.Packager.sh #lock file
NICE=which nice #nice
nice="/usr/bin/nice -n 12"
PHP=which php #php

#logs
log1="/var/log/my.log.1"
log2="/var/log/my.log.2"


#cleanup method
cleanup()
{
    rm -f ${LOCK}
    exit 0
}


#check if locked
if [ -f "${LOCK}" ]; then
    echo "$0 locked by ${LOCK}..."
    exit 1
fi

touch ${LOCK} #create a lock file
# </check if locked>

# Trapping interrrupt to provide correct cleanup.
trap cleanup SIGINT SIGTERM EXIT

#execute the external script
$nice -n 12 $php ./my.script.php 1>$log1 2>$log2
