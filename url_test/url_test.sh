#!/bin/bash


#variables
PROGNAME=$(basename $0)
FILE=             #file to process
OUTPUT='404.csv'  #output file
VERBOSE=0         #echo to screen
CURL=`which curl` 
GREP=`which grep`
URL=
DEBUG=0 #use to enable debug output
AGENT='Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0'           #agent string
MOBILE_AGENT='Mozilla/5.0 (Linux; Android 4.2.2; GT-I9505 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.92 Mobile Safari/537.36'
FOLLOW_REDIRECTS=


## Usage
function usage() {
cat << CIAO
NAME
    $PROGNAME - check a list of urls for 404 errors

SYNOPSIS
   ./$PROGNAME filename

DESCRIPTION
   checks al list of urls for 404 error

   Mandatory arguments to long options are mandatory for short options too.

   filename,         comma separated csv file with urls to check, 
                     first element in the file should be a valid url e.i. http://m.ciao.de
   -o, --output      output csv file , defaults to 404.csv
   -A, --user-agent  user agent string to use
   --follow          follow redirects
   -v, --verbose     put output also to console
   -h, --help        display this help and exit

EXAMPLES
   Check webmaster tools url for 404 list
   /$PROGNAME -f m-ciao-co-uk_20131108T101136Z_CrawlErrors.csv 

AUTHOR
     Written by Damijan Ćavar
CIAO
}

## Read input char
function readchar() {
    local __result=$1
    echo -en $2
    read -n 1 input
    eval $__result="'$input'"
    return 0
}


##validate input 
function validate_input() {
    # Exit if there is some invalid arguments
    if [ "$1" = "-h" ]; then
        usage
        exit 0
    fi

    if [ $# -lt 1 ]; then
        usage
        exit 0
    fi
}


# function takes first parameter as url 
# and get response time and http code returned
function checkurl() {
    local URL=$1
    local RESPONSE_TIME
    local HTTP_STATUS
    local LAST_FETCHED_URL
    local ret
    local line

    ret=`$CURL $URL --cookie non_existing_file $FOLLOW_REDIRECTS -A "\"$AGENT\"" -s -m 5 -o /dev/null -w %{time_total},%{http_code},%{url_effective}`
    RESPONSE_TIME=`echo $ret |awk -F',' '{print $1}'`
    HTTP_STATUS=`echo $ret |awk -F',' '{print $2}'`
    LAST_FETCHED_URL=`echo $ret |awk -F',' '{print $3}'`

    line="$URL,$RESPONSE_TIME,$HTTP_STATUS,$LAST_FETCHED_URL"
    line=${line//'\r'/''}
    line=${line//'\n'/''}
    output "$line"
}


#function takes url and 
function output() {
    local line=$1
    
    if [ "$VERBOSE" -eq "1" ]
    then
        echo $line
    fi    

    echo $line >> $OUTPUT
}


#END OF FUNCTIONS


#MAIN

#check user input
validate_input $@

#get the options
FILE=$1     #filename to process
shift

while [ $# -ne 0 ]; do
    #echo 'testing variable ' "$1"
    case "$1" in
          -o | --output)
            OUTPUT="$2"
            shift 2
            ;;
          -h | --help)
            usage
            exit 0
            ;;
          -v | --verbose)
            VERBOSE=1
            shift
            ;;
		  -A | user-agent)
		    AGENT=$2
            shift 2
            ;;
          --follow)
            FOLLOW_REDIRECTS='-L'
            shift
            ;;
          -*)
            echo "Error: Unknown option: $1" >&2
            exit 1
            ;;
          *)  # Unrecognised options
            usage
            exit 1
            ;;
    esac
done

if [ "$DEBUG" -eq 1 ]
then
    #debuging the options
    echo $FILE
    echo $VERBOSE
    echo $OUTPUT
    #exit
fi

#end of get options

#debuging the options
#echo $FILE
#echo $VERBOSE
#echo $OUTPUT
#exit

line_number=1

#chech if file exists
if [ -f $FILE ]
then
    #read the file line by line
    while read line
    do
        #skip the first line
        test $line_number -eq 1 && ((line_number=line_number+1)) && continue
        #get url from csv file
        URL=`echo $line |awk -F',' '{print $1}'`            
        checkurl $URL #check url
    done < "$FILE"
else
    echo "$FILE" 'does not exist'
fi

#END OF MAIN
